# Input: Integer
# Output: Return an integer with reveresed digits.
# Notes: Integer may be pos or neg
# Author: Matt Livingston
# Why this algorithm matters: This is just an intro algo that helps us master slicing skils in Python.
# A very basic algorithm.

val = input("Enter an integer (positive or negative): ")

string = str(val)

if string[0] == '-':
    print(int('-'+string[:0:-1]))
else:
    print(int(string[::-1]))

