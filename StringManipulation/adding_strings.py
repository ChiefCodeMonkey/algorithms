# Given two non-negative ints, return the sum of the two
# Note: num1 and num2 contains only digits 0-9
#       num1 and num2 do not contain any leading zeros.
# Author: Matt Livingston

num1 = input("Please enter the first number: ")
num2 = input("Please enter the second number: ")

print("Calculating using Eval()")
result = eval(str(num1)) + eval(str(num2))
print("")
print("Result = " + str(result))
print("")
print("Calculating using Ord()")

n1,n2 = 0,0
m1,m2 = 10**(len(str(num1))-1), 10**(len(str(num2))-1)

for i in str(num1):
    n1 += (ord(i) - ord("0")) * m1
    m1 = m1//10

for i in str(num2):
    n2 += (ord(i) - ord("0")) * m2
    m2 = m2//10

print("Result: " + str(n1 + n2))



