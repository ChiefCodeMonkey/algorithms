# Input: A sentence
# Output: Avg word length for the sentence.
# Why this is important: Performing string calculations is helpful in NLP.
# Author: Matt Livingston

sentence = input("Enter a sentence: ")

#remove punctuation
for p in "!?',:.":
    sentence = sentence.replace(p,'')

words = sentence.split()

avg = round(sum(len(word) for word in words)/len(words),2)

print(avg)
