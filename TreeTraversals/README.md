Binary Trees and their Traversals

Unlike linear data structures like Arrays, Linked Lists, Queues, Stacks, etc that only have one logical path to traverse them, trees can be traversed in many different ways. 

Given the following Tree:

                                   1
				/    \
                               2      3
                             /  \  
                            4    5


Depth First Traversals

(a) In Order (Left, Root, Right): 4,2,5,1,3
(b) Pre Order (Root, Left, Right): 1,2,4,5,3
(c) Post Order (Left, Right, Root): 4,5,2,3,1


Breadth First (Or Level Order): 1,2,3,4,5


PreOrder Traversal

Algorithm

1. Visit the Root.
2. Traverse the left subtree
3. Traverse the right subtree

Uses of Preorder

Preorder traversal is used to create a copy of a tree. It can also be used to be the prefix expression of an expression tree. 

