from __future__ import annotations
import queue


class TreeNode:
    def __init__(self, data):
        self.data = data
        self.right = None
        self.left = None


def build_tree():
    print("Enter an S to stop")
    check_node = input("Enter the value of the root node: ").strip().lower() or "n"
    if check_node == 'n':
        return None

    q: queue.Queue = queue.Queue()
    tree_node = TreeNode(int(check_node))
    q.put(tree_node)

    while not q.empty():
        node_found = q.get()
        msg = "Enter the left node of %s: or 'n' to stop " % node_found.data
        check_node = input(msg).strip().lower() or "n"
        if check_node == 'n':
            return tree_node
        left_node = TreeNode(int(check_node))
        node_found.left = left_node
        q.put(left_node)

        msg = "Enter the right node of %s: or 'n' to stop " % node_found.data
        check_node = input(msg).strip().lower() or "n"
        if check_node == 'n':
            return tree_node
        right_node = TreeNode(int(check_node))
        node_found.right = right_node
        q.put(right_node)

def pre_order(node: TreeNode) -> None:
    """
    root = TreeNode(1)
    tree_node2 = TreeNode(2)
    tree_node3 = TreeNode(3)
    tree_node4 = TreeNode(4)
    tree_node5 = TreeNode(5)
    tree_node6 = TreeNode(6)
    tree_node7 = TreeNode(7)
    root.left, root.right = tree_node2, tree_node3
    tree_node2.left, tree_node3.right = tree_node4, tree_node5
    tree_node3.left, tree_node3.right = tree_node6, tree_node7
    pre_order(root)
    1,2,3,4,5,6,7
    """

    if not isinstance(node, TreeNode) or not node:
        return
    print(node.data, end=",")
    pre_order(node.left)
    pre_order(node.right)





if __name__=="__main__":
    import doctest

    doctest.testmod()
    print("Binary Tree Traversals")

    node = build_tree()
    print("Pre Order Traversal")
    pre_order(node)
    print("\n")

