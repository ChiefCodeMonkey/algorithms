Neural Networks (NN), or also known as Artificial Neural Networks (ANN), are a subset of learning algorithms withing the machine learning field that are loosely based on the concept of biological neural networks.

An ANN is comprised of the following components:

1. An input layers that recieves data and passes it on
2. A hidden layer
3. An output layer
4. Weights between the layers
5. A deliberate activation function for every hidden layer. 

There are several types of neural networks. 




A Simple Neural Network

In this file, you will find an ANN that employs the Sigmoid activation function. This is a feed-forward, or, perception neural nnetwork. This type of ANN relays data directly from the font to the back.

Training the feed-forward neurons often need back propogation, which provides the network with corresponding sets of inputs and outputs. When the data is transmitted into the neuron, it is processed, and an output is generated. 
