"""

    Convert a string of characters into a string of 
    number corresponding to their position in the alphabet.

"""
from __future__ import annotations



def about() -> str:
    return """
    The A1Z26 Cipher uses numbers 1–26 as an index.
    A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
    1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26    
    
    This class of cipher is way to simple to be considered effective, but its
    a fun exercise nonetheless.
    """

def encode(plain: str) -> list[int]:
    """
        >>>encode("myname")
        [13, 25, 14, 1, 12, 5]
    """
    return [ord(elem) - 96 for elem in plain]

def decode(encoded: list[int]) -> str:
    """
        >>> decode([13, 25, 14, 1, 13, 5])
        'myname'
    """
    return "".join(chr(elem + 96) for elem in encoded)

def main() -> None:
    encoded = encode(input("-> ").strip().lower())
    print("Encoded: ", encoded)
    print("Decoded: ", decode(encoded))
    print(about())

if __name__ == "__main__":
    main()

